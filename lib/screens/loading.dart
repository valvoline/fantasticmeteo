import 'package:fantastic_meteo/shared/loader.dart';
import 'package:flutter/material.dart';
import 'package:fantastic_meteo/services/weather.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  String status = 'loading';

  void setupWeather() async {
    WeatherService instance = WeatherService(location: "London");
    await instance.getWeather();
    Navigator.pushReplacementNamed(context, '/home', arguments: instance);
  }

  @override
  void initState() {
    super.initState();
    setupWeather();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(50),
        child: loader,
      ),
    );
  }
}
