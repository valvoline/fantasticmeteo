import 'package:fantastic_meteo/services/weather.dart';
import 'package:fantastic_meteo/shared/loader.dart';
import 'package:flutter/material.dart';

class ChooseLocation extends StatefulWidget {
  ChooseLocation({Key key}) : super(key: key);

  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
  List<WeatherService> locations = [
    WeatherService(location: 'Ragusa'),
    WeatherService(location: 'London'),
    WeatherService(location: 'Rome'),
    WeatherService(location: 'New York'),
    WeatherService(location: 'San Francisco'),
    WeatherService(location: 'Paris'),
    WeatherService(location: 'Berlin'),
    WeatherService(location: 'Amsterdam')
  ];
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Choose a location'),
        centerTitle: true,
        backgroundColor: Colors.indigoAccent,
      ),
      body: Stack(
        children: <Widget>[
          ListView.builder(
            itemCount: locations.length,
            itemBuilder: (context, index) {
              return Card(
                child: ListTile(
                  onTap: () async {
                    setState(() => isLoading = true);
                    WeatherService instance = locations[index];
                    await instance.getWeather();
                    setState(() => isLoading = false);
                    Navigator.of(context).pop(instance);
                  },
                  title: Text(locations[index].location),
                ),
              );
            },
          ),
          isLoading == true
              ? loader
              : SizedBox(
                  height: 0.0,
                )
        ],
      ),
    );
  }
}
