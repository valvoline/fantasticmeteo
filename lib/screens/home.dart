import 'package:fantastic_meteo/services/weather.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  WeatherService data;

  @override
  Widget build(BuildContext context) {
    data = data != null ? data : ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: (data != null)
          ? (data.isDayTime == true)
              ? Colors.lightBlue[200]
              : Colors.indigo[700]
          : Colors.grey[200],
      body: Container(
          padding: EdgeInsets.all(60),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton.icon(
                  onPressed: () async {
                    dynamic result =
                        await Navigator.of(context).pushNamed('/location');
                    setState(() {
                      data = result;
                    });
                  },
                  icon: Icon(
                    Icons.location_on,
                    color: Colors.white,
                  ),
                  label: Text(
                    'Edit location',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                Text(
                  data != null ? data.location : '',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  data != null ? data.description : '',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                Text(
                  data != null
                      ? 'min ${data.tempMin}° - max ${data.tempMax}°'
                      : '',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 12.0),
                Image.network(data.image)
              ],
            ),
          )),
    );
  }
}
