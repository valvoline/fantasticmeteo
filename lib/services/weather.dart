import 'package:http/http.dart';
import 'dart:convert';

class WeatherService {
  // https://api.openweathermap.org/data/2.5/find?q=London&units=metric&appid=b6907d289e10d714a6e88b30761fae22
  final String apiKey = 'bce975754aef1d1ca1246b9b2c3451dc';
  final String baseURL = 'https://api.openweathermap.org/data/2.5/weather?q=';
  String location;
  String message;
  String tempMin;
  String tempMax;
  String description;
  String image;
  bool isDayTime;
  bool hasError;

  WeatherService({this.location});

  Future getWeather() async {
    String requestURL = baseURL + location + '&units=metric&appid=' + apiKey;
    try {
      Response response = await get(requestURL);
      // print(response.body);
      var json = jsonDecode(response.body);
      if (json["weather"] != null && json["main"] != null) {
        List<dynamic> weatherList = json["weather"];
        var mainInfos = json["main"];
        var weatherInfo = weatherList.first;
        tempMin = mainInfos["temp_min"].toString();
        tempMax = mainInfos["temp_max"].toString();
        description = weatherInfo["description"];
        var lastCharOfIcon = weatherInfo["icon"];
        lastCharOfIcon = lastCharOfIcon.substring(lastCharOfIcon.length - 1);
        isDayTime = (lastCharOfIcon == 'd') ? true : false;
        image = 'http://openweathermap.org/img/wn/' +
            weatherInfo["icon"] +
            '@2x.png';
        message = "";
        hasError = false;
      } else {
        throw Exception('json format error');
      }
    } catch (e) {
      print(e.toString());
      message = e.toString();
      hasError = true;
    }
  }
}
